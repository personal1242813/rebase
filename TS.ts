
declare interface FieldPacket {
    constructor: {
        name: 'FieldPacket'
    };
    catalog: string;
    charsetNr: number;
    db: string;
    decimals: number;
    table: string;
    type: number;
    zerofill: boolean;
}
